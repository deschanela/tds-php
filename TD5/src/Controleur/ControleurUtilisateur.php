<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Utilisateur as ModeleUtilisateur;

class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        //appel au modèle pour gérer la BD
        $utilisateurs = Utilisateur::recupererUtilisateurs();
        $titre = "Liste des utilisateurs";
        $cheminCorpsVue = "liste.php";
        self::afficherVue(
            '../vue/utilisateur/vueGenerale.php',
            ["utilisateurs" => $utilisateurs, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]
        );
    }
    public static function afficherDetail() : void {
        $login = $_GET["login"];
        if ($login && Utilisateur::recupererUtilisateurParLogin($login)) {
            $utilisateur = Utilisateur::recupererUtilisateurParLogin($login); //appel au modèle pour gérer la BD
            self::afficherVue(
                '../vue/utilisateur/vueGenerale.php',
                ["utilisateur" => $utilisateur, "titre" => "Détail utilisateur", "cheminCorpsVue" => "detail.php"]
            );
        }else {
            self::afficherVue(
                '../vue/utilisateur/vueGenerale.php',
                ["titre" => "Erreur", "cheminCorpsVue" => "erreur.php"]
            );
        }
    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue(
            '../vue/utilisateur/vueGenerale.php',
            [ "titre" => "Creation utilisateur", "cheminCorpsVue" => "formulaireCreation.php"]
        );
    }
    public static function creerDepuisFormulaire() : void {
        $utilisateurs = Utilisateur::recupererUtilisateurs();
        $login=$_GET['login'];$nom=$_GET['nom'];$prenom=$_GET['prenom'];
        $utilisateur = Utilisateur::construireDepuisTableauSQL([$login, $nom, $prenom]);
        $utilisateur->ajouter();
        self::afficherVue('../vue/utilisateur/vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateurCree.php", "utilisateurs" => $utilisateurs]);
    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
}