<?php

use App\Covoiturage\Controleur\ControleurGenerique;
use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Controleur\ControleurTrajet;
use App\Covoiturage\Lib\PreferenceControleur;

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';


// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

if (!isset($_GET["controleur"])) {
    if (PreferenceControleur::existe()){
        $controleur = PreferenceControleur::lire();
    }else{
        $controleur = "utilisateur";
    }
}else{
    $controleur = $_GET["controleur"];
}
$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur".ucfirst($controleur);

if (!class_exists($nomDeClasseControleur)) {
    ControleurGenerique::afficherErreur("La page que vous chercher à atteindre n'existe pas");
}else {
    if (!isset($_GET["action"])) {
        $action = "afficherListe";
    }else{
        $action = $_GET["action"];
    }

    $possibiliteU = get_class_methods(ControleurUtilisateur::class);

    $possibiliteT = get_class_methods(ControleurTrajet::class);

    if (in_array($action, $possibiliteU) and $controleur=="utilisateur") {
        ControleurUtilisateur::$action();
    } elseif (in_array($action, $possibiliteT) and $controleur=="trajet") {
        ControleurTrajet::$action();
    } else {
        ControleurGenerique::afficherErreur("La page que vous chercher à atteindre n'existe pas");
    }

}
