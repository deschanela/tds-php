<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique {
    protected static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
    public static function afficherErreur(string $messageErreur = "") : void {
        if ($messageErreur == ""){
            $messageErreur = "Problème controleur inconue";
        }
        if (!isset($_GET['controleur'])){
            $controleur = "utilisateur";
        }else {$controleur = $_GET['controleur'];}

        self::afficherVue("../vue/vueGenerale.php", ["titre" => "Erreur", "cheminCorpsVue" => "$controleur/erreur.php", "messageErreur" => $messageErreur]);
    }
    public static function afficherFormulairePreference (){
        self::afficherVue("../vue/vueGenerale.php", ["titre" => "Formulaire Preference", "cheminCorpsVue" => "formulairePreference.php"]);
    }

    /** @var String $controleur */
    public static function enregistrerPreference() : void {
        $controleur = $_GET["controleur_defaut"];
        PreferenceControleur::enregistrer($controleur);
        self::afficherVue("../vue/vueGenerale.php", ["titre" => "Formulaire Preference", "cheminCorpsVue" => "preferenceEnregistree.php"]);

    }
}