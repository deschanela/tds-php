<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurUtilisateur extends ControleurGenerique {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        //appel au modèle pour gérer la BD
        $utilisateurs = (new UtilisateurRepository)->recuperer();
        $titre = "Liste des utilisateurs";
        $cheminCorpsVue = "utilisateur/liste.php";
        self::afficherVue(
            '../vue/vueGenerale.php',
            ["utilisateurs" => $utilisateurs, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]
        );
    }
    public static function afficherDetail() : void {
        $login = $_GET["login"];
        if ($login && (new UtilisateurRepository())->recupererParClePrimaire($login)) {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
            self::afficherVue(
                '../vue/vueGenerale.php',
                ["utilisateur" => $utilisateur, "titre" => "Détail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]
            );
        }else {
            self::afficherErreur("utilisateur inexistant");
        }
    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue(
            '../vue/vueGenerale.php',
            [ "titre" => "Creation utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]
        );
    }
    public static function creerDepuisFormulaire() : void {
        if ($_GET["mdp"] == $_GET["mdp2"]){
            $utilisateur = self::construireDepuisFormulaire(["login" => $_GET["login"], "nom" => $_GET["nom"], "prenom" => $_GET["prenom"], "mdpHache" => $_GET["mdp"]]);
            (new UtilisateurRepository)->ajouter($utilisateur);
            $utilisateurs = (new UtilisateurRepository)->recuperer();
            self::afficherVue('../vue/vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "utilisateurs" => $utilisateurs]);
        }else{
            self::afficherErreur("Mots de passe distincts");
        }
    }
    public static function supprimer() : void {
        if (isset($_GET["login"])) {
            $login = $_GET["login"];
            if (isset($utilisateur) && $login == ConnexionUtilisateur::getLoginUtilisateurConnecte()) {
                (new UtilisateurRepository)->supprimer($login);
                $utilisateurs = (new UtilisateurRepository)->recuperer();
                self::afficherVue("../vue/vueGenerale.php", ["titre"=>"Liste des utilisateurs", "cheminCorpsVue"=>"utilisateur/utilisateurSupprime.php", "login"=>$login, "utilisateurs"=>$utilisateurs]);
            }else {
                self::afficherErreur();
            }
        } else {
            self::afficherErreur();
        }
    }

    //public static function deposerCookie () { Cookie::enregistrer("clef", "bleu", 1);}
    //public static function lireCookie(){echo Cookie::lire("clef");}
    public static function afficherFormulaireMiseAJour() : void {
        if (isset($_GET["login"])) {
            $login = $_GET["login"];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            self::afficherVue("../vue/vueGenerale.php", ["titre"=>"Formulaire de mise a jour", "cheminCorpsVue"=>"utilisateur/formulaireMiseAJour.php", "utilisateur"=>$utilisateur]);
        } else {
            self::afficherErreur();
        }
    }
    public static function mettreAJour() : void{
        $login = $_GET["login"]; $nom = $_GET["nom"]; $prenom = $_GET["prenom"];
        $mdpHache = $_GET["mdp"]; $mdpHacheVerification = $_GET["mdp2"]; $mdpa = $_GET["amdp"];
        if (isset($login) && isset($nom) && isset($prenom) && isset($mdpHache) && isset($mdpHacheVerification) && isset($mdpa)) {
            /** @var Utilisateur $utilisateur */
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if (isset($utilisateur) && $login == ConnexionUtilisateur::getLoginUtilisateurConnecte()) {
                if ($mdpHache == $mdpHacheVerification && MotDePasse::verifier($mdpa, $utilisateur->getMdpHache())) {
                    $temp = self::construireDepuisFormulaire(["login" => $login, "nom" => $nom, "prenom"=> $prenom, "mdpHache" => $mdpHache]);
                    (new UtilisateurRepository)->mettreAJour($temp);
                    $utilisateurs = (new UtilisateurRepository)->recuperer();
                    self::afficherVue("../vue/vueGenerale.php", ["titre"=>"Liste des utilisateurs", "cheminCorpsVue"=>"utilisateur/utilisateurMisAJour.php", "utilisateurs"=>$utilisateurs,"login"=> $login]);
                } else {
                    self::afficherErreur("Mot de passe incorrecte");
                }
            }else {
                self::afficherErreur("Login incorrecte");
            }
        } else {
            self::afficherErreur("Information manquante");
        }
    }
    public static function afficherFormulaireConnexion(): void {
        self::afficherVue("../vue/vueGenerale.php", ["titre"=>"Connexion", "cheminCorpsVue"=>"utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter() : void {
        if (isset($_GET["login"]) && isset($_GET["mdp"])){
            $login = $_GET["login"]; $mdp = $_GET["mdp"];
            /** @var Utilisateur $utilistateur */
            $utilistateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if (MotDePasse::verifier($mdp, $utilistateur->getMdpHache())) {
                ConnexionUtilisateur::connecter($login);
                self::afficherVue("../vue/vueGenerale.php", ["titre"=>"Détail Utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php", "login"=>$login, "utilisateur"=>$utilistateur]);
            } else {
                self::afficherErreur("Login et/ou mot de passe incorrect");
            }
        }else {
            self::afficherErreur("Login et/ou mot de passe manquant");
        }
    }

    public static function deconnecter() : void {
        ConnexionUtilisateur::deconnecter();
        $utilisateurs = (new UtilisateurRepository)->recuperer();
        self::afficherVue("../vue/vueGenerale.php", ["titre"=>"Liste des utilisateurs", "cheminCorpsVue"=>"utilisateur/utilisateurDeconnecte.php", "utilisateurs"=>$utilisateurs]);
    }

    /**
     * @return Utilisateur
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur {
        return new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["nom"], $tableauDonneesFormulaire["prenom"], MotDePasse::hacher($tableauDonneesFormulaire["mdpHache"]));
    }
}