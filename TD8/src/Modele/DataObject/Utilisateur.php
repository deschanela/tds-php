<?php
namespace App\Covoiturage\Modele\DataObject;

class Utilisateur extends AbstractDataObject{

    private string $login;
    private string $nom;
    private string $prenom;
    private string $mdpHache;

    public function getNom(): string { return $this->nom; }
    public function setNom(string $nom) { $this->nom = $nom; }

    public function getPrenom(): string { return $this->prenom; }
    public function setPrenom(string $prenom) { $this->prenom = $prenom; }

    public function getLogin() { return $this->login; }
    public function setLogin(string $login) { $this->login = substr($login, 0, 64); }

    public function getMdpHache() { return $this->mdpHache; }
    public function setMdpHache() { $this->mdpHache = substr($this->mdpHache, 0, 256); }


    public function __construct( string $login, string $nom, string $prenom, string $mdpHache ) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->mdpHache = substr($mdpHache, 0, 256);
    }

    /**public function __toString() {
     * * return "Login : $this->login <br>Nom : $this->nom <br>Prenom : $this->prenom";
     * }
     */
}

