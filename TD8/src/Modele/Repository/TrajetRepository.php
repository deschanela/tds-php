<?php
namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class TrajetRepository extends AbstractRepository {
    protected function construireDepuisTableauSQL(array $objectFormatTableau) : Trajet {
        $conducteurLogin =(new UtilisateurRepository()) -> recupererParClePrimaire($objectFormatTableau["conducteurLogin"]);

        $trajet = new Trajet(
            $objectFormatTableau["id"],
            $objectFormatTableau["depart"],
            $objectFormatTableau["arrivee"],
            DateTime::createFromFormat("Y-m-d", $objectFormatTableau["date"]),
            $objectFormatTableau["prix"],
            $conducteurLogin,
            $objectFormatTableau["nonFumeur"]
        );
        $trajet->setPassagers(self::recupererPassagers($trajet));
        return $trajet;
    }

    /**
     * @return Utilisateur[]
     */
    private static function recupererPassagers(Trajet $trajet): array{
        $sql = "SELECT DISTINCT passagerlogin from passager WHERE trajetid = ".$trajet->getId();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query($sql);
        $utilisateurFormatTableau = $pdoStatement->fetchAll();
        $tableau = [];
        if ($utilisateurFormatTableau){
            foreach ($utilisateurFormatTableau as $utilisateur){
                $tableau[] = (new UtilisateurRepository())->recupererParClePrimaire($utilisateur[0]);
            }
        }
        return $tableau;
    }
    protected function getNomTable(): string { return "trajet"; }

    protected function getNomClePrimaire(): string{
        return "id";
    }
    /** @return string[] */
    protected function getNomsColonnes(): array {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array{
        /** @var Trajet $trajet */
        if ($trajet->isNonFumeur()){
            $nonFumeur = 1;
        }else {
            $nonFumeur = 0;
        }
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" =>  $nonFumeur
        );
    }
}