<!DOCTYPE html>
<html>
<body>
<?php
use App\Covoiturage\Lib\ConnexionUtilisateur;

/** @var Utilisateur $utilisateur */

echo '<p>login : ' . htmlspecialchars($utilisateur->getLogin()) .
     '<br>  nom : ' . htmlspecialchars($utilisateur->getNom()) .
     '<br>  prenom : ' . htmlspecialchars($utilisateur->getPrenom()) .
     ' </p>';

if (ConnexionUtilisateur::estConnecte() && ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
    echo "<p><a href='../web/controleurFrontal.php?action=supprimer&controleur=utilisateur&login=".rawurlencode($utilisateur->getLogin())."'>supprimer</a> <a href='../web/controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur&login=".rawurlencode($utilisateur->getLogin())."'>modifier</a></p>";
}
?>
</body>
</html>