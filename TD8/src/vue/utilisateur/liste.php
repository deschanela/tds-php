<!DOCTYPE html>
<html>
    <body>
        <div>
        <?php
            /**
             * @var ModeleUtilisateur[] $utilisateurs
             */
            foreach ($utilisateurs as $utilisateur)
                echo "<p> Utilisateur de login " .htmlspecialchars($utilisateur->getLogin()) . " <a href='../web/controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=".rawurlencode($utilisateur->getLogin())."'>(voir plus)</a> </p>";
        ?>
        </div>
        <br>
        <div>
            <p>
                <a href="../web/controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur">Nouvel Utilisateur</a>
            </p>
        </div>

    </body>
</html>