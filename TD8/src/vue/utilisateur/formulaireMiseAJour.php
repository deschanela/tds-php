<?php
/** @var Utilisateur $utilisateur */
$login = $utilisateur -> getLogin();
$nom = $utilisateur -> getNom();
$prenom = $utilisateur -> getPrenom();
?>
<!DOCTYPE html>
<html>
<body>
<form method="get" action='controleurFrontal.php'>
    <input type='hidden' name='action' value='mettreAJour'>
    <input type="hidden" name="controleur" value="utilisateur">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label> :
            <?= "<input class='InputAddOn-field' type='text' value='$login' name='login' id='login_id' required readonly/>"?>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom</label> :
            <?= "<input class='InputAddOn-field' type='text' value='$nom' name='nom' id='nom_id' required/>" ?>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom</label> :
            <?= "<input class='InputAddOn-field' type='text' value='$prenom' name='prenom' id='prenom_id' required/>" ?>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="amdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>

</html>
