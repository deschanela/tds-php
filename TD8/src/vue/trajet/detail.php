<!DOCTYPE html>
<html>
    <body>
        <?php
        /** @var Trajet $trajet
         ** @var array $passagers*/

        if ($trajet->isNonFumeur() == 1){
            $nonFumer = "non fumeur";
        }else{
            $nonFumer = "fumeur";
        }
        echo '<p>[Trajet '.$trajet->getId().' par '.
              htmlspecialchars($trajet->getConducteur()->getLogin()).
             ' |  Depart de ' . htmlspecialchars($trajet->getDepart()) .
             ',  arrivée à '.htmlspecialchars($trajet->getArrivee()).
             ' le '. $trajet->getDate()->format("d-m-Y").', prix '.
              htmlspecialchars($trajet->getPrix()).'€, '.$nonFumer.']</p>';

        if (empty($passagers)) {
            echo "<p>Aucun passager pour ce trajet.</p>";
        } else {
            foreach ($passagers as $passager) {
                echo '<p>[ Passager ' . htmlspecialchars($passager->getLogin()) . ' ]</p>';
            }
        }
        ?>

    </body>
</html>