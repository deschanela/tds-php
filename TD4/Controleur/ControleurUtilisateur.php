<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        //appel au modèle pour gérer la BD
        $utilisateurs = [];
        $utilisateurs[] = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue(
            '../vue/utilisateur/liste.php',
            $utilisateurs
        );
    }
    public static function afficherDetail() : void {
        $login = $_GET["login"];
        if ($login && ModeleUtilisateur::recupererUtilisateurParLogin($login)) {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login); //appel au modèle pour gérer la BD
            ControleurUtilisateur::afficherVue('../vue/utilisateur/detail.php', [$utilisateur]);  //"redirige" vers la vue
        }else {
            ControleurUtilisateur::afficherVue('../vue/utilisateur/erreur.php');
        }
    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue(
            '../vue/utilisateur/formulaireCreation.php'
        );
    }
    public static function creerDepuisFormulaire() : void {
        $login=$_GET['login'];$nom=$_GET['nom'];$prenom=$_GET['prenom'];
        $utilisateur = ModeleUtilisateur::construireDepuisTableauSQL([$login, $nom, $prenom]);
        $utilisateur->ajouter();
        self::afficherListe();
    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }
}