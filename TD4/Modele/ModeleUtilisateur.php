<?php
require_once "ConnexionBaseDeDonnees.php";

class ModeleUtilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    public function getNom(): string { return $this->nom; }
    public function setNom(string $nom) { $this->nom = $nom; }

    public function getPrenom(): string { return $this->prenom; }
    public function setPrenom(string $prenom) { $this->prenom = $prenom; }

    public function getLogin() { return $this->login; }
    public function setLogin(string $login) { $this->login = substr($login, 0, 64); }


    public function __construct( string $login, string $nom, string $prenom ) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }
    public static function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $login,
        );
        $pdoStatement->execute($values);
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if ($utilisateurFormatTableau){
            return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }else {
            return null;
        }
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : ModeleUtilisateur {
         return new ModeleUtilisateur($utilisateurFormatTableau[0], $utilisateurFormatTableau[1], $utilisateurFormatTableau[2]);
    }

    public static function recupererUtilisateurs() {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "Select * From utilisateur";
        $pdoStatement = $pdo->query($sql);
        foreach ($pdoStatement as $users){
            $Tableau[] = ModeleUtilisateur::construireDepuisTableauSQL($users);
        }
        return $Tableau;
    }
    public function ajouter() : void {
        $sql = "Insert into utilisateur (login,nom,prenom) VALUES (:login, :nom, :prenom)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "login" => $this->login,
            "nom" => $this->nom,
            "prenom" => $this->prenom
        );
        $pdoStatement->execute($values);

    }
    /**public function __toString() {
     * * return "Login : $this->login <br>Nom : $this->nom <br>Prenom : $this->prenom";
     * }
     */
}

