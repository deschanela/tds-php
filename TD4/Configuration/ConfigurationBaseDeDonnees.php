<?php
class ConfigurationBaseDeDonnees {
    private static array $configurationBaseDeDonnees = array (
        "nomHote"=>"webinfo.iutmontp.univ-montp2.fr",
        "nomBaseDeDonnees" => "deschanela",
        "port" => "3316",
        "login"=>"deschanela",
        "motDePasse"=>"mot de passe"
    );

    static public function getNomHote() : string { return self::$configurationBaseDeDonnees["nomHote"]; }
    static public function getLogin() : string { return self::$configurationBaseDeDonnees["login"]; }
    static public function getNomBaseDeDonnes() : string { return self::$configurationBaseDeDonnees["nomBaseDeDonnees"]; }
    static public function getPassword() : string { return self::$configurationBaseDeDonnees["motDePasse"]; }
    static public function getPort() : string { return self::$configurationBaseDeDonnees["port"]; }
}