<!DOCTYPE html>
<html>
    <body>
        <div>
        <?php
            /**
             * @var ModeleUtilisateur[] $utilisateurs
             */
            foreach ($utilisateurs as $utilisateur)
                echo "<p> Utilisateur de login " .htmlspecialchars($utilisateur->getLogin()) . " <a href='../web/controleurFrontal.php?action=afficherDetail&login=".rawurlencode($utilisateur->getLogin())."'>(voir plus)</a> <a href='../web/controleurFrontal.php?action=supprimer&login=".rawurlencode($utilisateur->getLogin())."'>supprimer</a> <a href='../web/controleurFrontal.php?action=afficherFormulaireMiseAJour&login=".rawurlencode($utilisateur->getLogin())."'>modifier</a></p>";
        ?>
        </div>
        <br>
        <div>
            <p>
                <a href="../web/controleurFrontal.php?action=afficherFormulaireCreation">Nouvel Utilisateur</a>
            </p>
        </div>

    </body>
</html>