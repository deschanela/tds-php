<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use \DateTime;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;

class ControleurTrajet{
    public static function afficherListe() : void {
        //appel au modèle pour gérer la BD
        $trajets = (new TrajetRepository)->recuperer();
        $titre = "Liste des utilisateurs";
        $cheminCorpsVue = "liste.php";
        self::afficherVue(
            '../vue/trajet/vueGenerale.php',
            ["trajets" => $trajets, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]
        );
    }
    public static function afficherErreur(string $messageErreur = "") : void {
        if ($messageErreur == ""){
            $messageErreur = "Problème avec le trajet";
        }
        self::afficherVue("../vue/trajet/vueGenerale.php", ["titre" => "Erreur", "cheminCorpsVue" => "erreur.php", "messageErreur" => $messageErreur]);
    }
    public static function afficherDetail() : void {
        $id = $_GET["id"];
        if ($id && (new TrajetRepository())->recupererParClePrimaire($id)) {
            $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
            self::afficherVue(
                '../vue/trajet/vueGenerale.php',
                ["trajet" => $trajet, "titre" => "Détail trajet", "cheminCorpsVue" => "detail.php", "passagers" => $trajet->getPassagers()]
            );
        }else {
            self::afficherErreur("trajet inexistant");
        }
    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue(
            '../vue/trajet/vueGenerale.php',
            [ "titre" => "Creation Trajet", "cheminCorpsVue" => "formulaireCreation.php"]
        );
    }
    public static function supprimer() : void {
        if (isset($_GET["id"])) {
            $id = $_GET["id"];
            (new TrajetRepository())->supprimer($id);
            $trajets = (new TrajetRepository)->recuperer();
            self::afficherVue("../vue/trajet/vueGenerale.php", ["titre"=>"Liste des trajets", "cheminCorpsVue"=>"trajetSupprime.php", "id"=>$id, "trajets"=>$trajets]);
        } else {
            self::afficherErreur();
        }
    }
    public static function creerDepuisFormulaire() : void {
        $trajet = self::construireDepuisFormulaire(["depart" => $_GET["depart"], "arrivee" => $_GET["arrivee"], "prix" => $_GET["prix"], "nonFumeur" => isset($_GET["nonFumeur"]), "date" => DateTime::createFromFormat("Y-m-d",$_GET["date"]), "conducteurLogin" => (new UtilisateurRepository)->recupererParClePrimaire($_GET["conducteurLogin"])]);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();

        self::afficherVue('../vue/trajet/vueGenerale.php', ["titre" => "Liste des trajets", "cheminCorpsVue" => "creerTrajet.php", "trajets" => $trajets]);
    }
    public static function afficherFormulaireMiseAJour(){
        if (isset($_GET["id"])) {
            $id = $_GET["id"];
            $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
            self::afficherVue("../vue/trajet/vueGenerale.php", ["titre"=>"Formulaire de mise a jour", "cheminCorpsVue"=>"formulaireMiseAJour.php", "trajet"=>$trajet]);
        } else {
            self::afficherErreur();
        }
    }
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    /**
     * @return Trajet
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet{
        return new Trajet($tableauDonneesFormulaire["id"] ?? null, $tableauDonneesFormulaire["depart"], $tableauDonneesFormulaire["arrivee"], $tableauDonneesFormulaire["date"], $tableauDonneesFormulaire["prix"], $tableauDonneesFormulaire["conducteurLogin"], $tableauDonneesFormulaire["nonFumeur"]);
    }
    public static function mettreAJour() : void{
        if (!isset($_GET["nonFumeur"])){
            $nonFumeur = false;
        }else {
            $nonFumeur= isset($_GET["nonFumeur"]);
        }
        $id = $_GET["id"];
        if (isset($id)) {
            $temp = self::construireDepuisFormulaire(["id" => $_GET["id"], "depart" => $_GET["depart"], "arrivee" => $_GET["arrivee"], "prix" => $_GET["prix"], "nonFumeur" => $nonFumeur, "date" => DateTime::createFromFormat("Y-m-d",$_GET["date"]), "conducteurLogin" => (new UtilisateurRepository)->recupererParClePrimaire($_GET["conducteurLogin"])]);
            (new TrajetRepository())->mettreAJour($temp);
            $trajets = (new TrajetRepository())->recuperer();
            self::afficherVue("../vue/trajet/vueGenerale.php", ["titre"=>"Liste des trajets", "cheminCorpsVue"=>"trajetMisAJour.php", "trajets"=>$trajets,"id"=> $id]);
        } else {
            self::afficherErreur();
        }
    }
}