<?php
namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class UtilisateurRepository extends AbstractRepository {
    protected function construireDepuisTableauSQL(array $objectFormatTableau) : Utilisateur {
        return new Utilisateur($objectFormatTableau[0], $objectFormatTableau[1], $objectFormatTableau[2]);
    }
    protected function getNomTable(): string { return "utilisateur"; }

    protected function getNomClePrimaire(): string { return "login"; }

    /** @return string[] */
    protected function getNomsColonnes() : array{ return ["login", "nom", "prenom"];}

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
        );
    }
}