<?php
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    // un getter
    public function getPrenom(): string
    {
        return $this->prenom;
    }
    // un setter
    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }
    // un getter
    public function getLogin() {
        return $this->login;
    }
    // un setter
    public function setLogin(string $login) {
        $this->login = substr($login, 0, 64);
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
   ) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
         return new Utilisateur($utilisateurFormatTableau[0], $utilisateurFormatTableau[1], $utilisateurFormatTableau[2]);
    }

    public static function recupererUtilisateurs() {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "Select * From utilisateur";
        $pdoStatement = $pdo->query($sql);
        foreach ($pdoStatement as $users){
            $Tableau[] = Utilisateur::construireDepuisTableauSQL($users);
        }
        return $Tableau;
    }
    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() {
        // À compléter dans le prochain exercice
        return "Login : $this->login <br>Nom : $this->nom <br>Prenom : $this->prenom";
    }
}

