<?php
use App\Covoiturage\Lib\PreferenceControleur;

$checkedU = "";
$checkedT = "";
if (PreferenceControleur::existe()){
    if (PreferenceControleur::lire() == "utilisateur"){
        $checkedU = "checked";
    }else {
        $checkedT = "checked";
    }
}
?>
<!DOCTYPE html>
<html>
    <body>
    <form method="get" action='controleurFrontal.php'>
        <input type='hidden' name='action' value='enregistrerPreference'>
        <fieldset>
            <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?=$checkedU?>>
            <label for="utilisateurId">Utilisateur</label>
            <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?=$checkedT?>>
            <label for="trajetId">Trajet</label>
            <br>
            <input type="submit" value="Envoyer">
        </fieldset>
    </form>
    </body>
</html>