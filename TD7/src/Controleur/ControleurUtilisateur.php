<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurUtilisateur extends ControleurGenerique {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        //appel au modèle pour gérer la BD
        $utilisateurs = (new UtilisateurRepository)->recuperer();
        $titre = "Liste des utilisateurs";
        $cheminCorpsVue = "utilisateur/liste.php";
        self::afficherVue(
            '../vue/vueGenerale.php',
            ["utilisateurs" => $utilisateurs, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]
        );
    }
    public static function afficherDetail() : void {
        $login = $_GET["login"];
        if ($login && (new UtilisateurRepository())->recupererParClePrimaire($login)) {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
            self::afficherVue(
                '../vue/vueGenerale.php',
                ["utilisateur" => $utilisateur, "titre" => "Détail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]
            );
        }else {
            self::afficherErreur("utilisateur inexistant");
        }
    }
    public static function afficherFormulaireCreation() : void {
        self::afficherVue(
            '../vue/vueGenerale.php',
            [ "titre" => "Creation utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]
        );
    }
    public static function creerDepuisFormulaire() : void {
        $utilisateur = self::construireDepuisFormulaire(["login" => $_GET["login"], "nom" => $_GET["nom"], "prenom" => $_GET["prenom"]]);
        (new UtilisateurRepository)->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository)->recuperer();
        self::afficherVue('../vue/vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "utilisateurs" => $utilisateurs]);
    }
    public static function supprimer() : void {
        if (isset($_GET["login"])) {
            $login = $_GET["login"];
            (new UtilisateurRepository)->supprimer($login);
            $utilisateurs = (new UtilisateurRepository)->recuperer();
            self::afficherVue("../vue/vueGenerale.php", ["titre"=>"Liste des utilisateurs", "cheminCorpsVue"=>"utilisateur/utilisateurSupprime.php", "login"=>$login, "utilisateurs"=>$utilisateurs]);
        } else {
            self::afficherErreur();
        }
    }

    //public static function deposerCookie () { Cookie::enregistrer("clef", "bleu", 1);}
    //public static function lireCookie(){echo Cookie::lire("clef");}
    public static function afficherFormulaireMiseAJour() : void {
        if (isset($_GET["login"])) {
            $login = $_GET["login"];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            self::afficherVue("../vue/vueGenerale.php", ["titre"=>"Formulaire de mise a jour", "cheminCorpsVue"=>"utilisateur/formulaireMiseAJour.php", "utilisateur"=>$utilisateur]);
        } else {
            self::afficherErreur();
        }
    }
    public static function mettreAJour() : void{
        $login = $_GET["login"];
        if (isset($login)) {
            $temp = self::construireDepuisFormulaire(["login" => $_GET["login"], "nom" => $_GET["nom"], "prenom"=> $_GET["prenom"]]);
            (new UtilisateurRepository)->mettreAJour($temp);
            $utilisateurs = (new UtilisateurRepository)->recuperer();
            self::afficherVue("../vue/vueGenerale.php", ["titre"=>"Liste des utilisateurs", "cheminCorpsVue"=>"utilisateur/utilisateurMisAJour.php", "utilisateurs"=>$utilisateurs,"login"=> $login]);
        } else {
            self::afficherErreur();
        }
    }

    /**
     * @return Utilisateur
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur {
        return new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["nom"], $tableauDonneesFormulaire["prenom"]);
    }
}