<?php
namespace App\Covoiturage\Modele\DataObject;

class Utilisateur extends AbstractDataObject{

    private string $login;
    private string $nom;
    private string $prenom;

    public function getNom(): string { return $this->nom; }
    public function setNom(string $nom) { $this->nom = $nom; }

    public function getPrenom(): string { return $this->prenom; }
    public function setPrenom(string $prenom) { $this->prenom = $prenom; }

    public function getLogin() { return $this->login; }
    public function setLogin(string $login) { $this->login = substr($login, 0, 64); }


    public function __construct( string $login, string $nom, string $prenom ) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**public function __toString() {
     * * return "Login : $this->login <br>Nom : $this->nom <br>Prenom : $this->prenom";
     * }
     */
}

